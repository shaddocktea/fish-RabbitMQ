package com.fish.rabbitmq.direct;

import com.rabbitmq.client.*;

/**
 * @ClassName Consumer
 * @Description 路由(direct)模式/消费者
 * @Author PomeloTea
 * @Date 2021/9/15
 */
public class Consumer {

    // 定义交换机名称
    public static final String EXCHANGE_NAME = "direct-exchange";

    public static Runnable runnable = () -> {
        // 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置主机IP
        factory.setHost("122.9.98.31");
        // 设置端口号
        factory.setPort(5672);
        // 设置虚拟访问节点
        factory.setVirtualHost("/");
        // 设置登录用户
        factory.setUsername("admin");
        // 设置密码
        factory.setPassword("admin");

        // 获取线程的名称并设置为当前的队列名称
        final String queueName = Thread.currentThread().getName();

        Connection connection = null;
        Channel channel = null;
        try {
            // 创建连接
            connection = factory.newConnection("消费者");
            // 创建信道
            channel = connection.createChannel();

            // 消息送达时通知的回调接口
            DeliverCallback deliverCallback = (consumerTag, message) -> {
                // 获取消息体
                String strMessage = new String(message.getBody());
                System.out.println(queueName + "收到的消息：" + strMessage);
            };

            // 取消消费的回调接口
            CancelCallback cancelCallback = consumerTag -> {
                System.out.println("消费消息被中断！");
            };

            /*
             消费者消费消息
             参数：
                queue – 消费的队列名称
                autoAck – 设置消息消费成功之后是自动应答还是手动应答 true：自动 false：手动
                DeliverCallback – 传递消息时的回调
                cancelCallback – 消费者取消时的回调
            */
            channel.basicConsume(queueName, true, deliverCallback, cancelCallback);

            System.out.println(queueName + ": 开始接收消息");
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("消息接收异常！");
        } finally {
            //关闭信道
            if (null != channel && channel.isOpen()) {
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //关闭连接
            if (null != connection && connection.isOpen()) {
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public static void main(String[] args) {
        // 启动三个线程去接收消息
        new Thread(runnable,"queue-direct-001").start();
        new Thread(runnable,"queue-direct-002").start();
        new Thread(runnable,"queue-direct-003").start();

    }
}
