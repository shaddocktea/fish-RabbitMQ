package com.fish.rabbitmq.simple;

import com.fish.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;


/**
 * @ClassName Producer
 * @Description 生产者
 * @Author PomeloTea
 * @Date 2021/9/10
 */
public class Producer {

    //队列名称
    public static final String QUEUE_NAME = "hello";

    //创建需要发送的消息对象
    public static final String MESSAGE = "Good night!";

    public static void main(String[] args) {

        //通过工具类获取连接对象
        Connection connection = RabbitMqUtils.getConnection();
        //获取连接通道
        Channel channel = null;
        try {
            channel = connection.createChannel();

            //通道绑定消息队列
            //参数1:队列名称
            //参数2:用于定义队列是否持久化 true 持久化队列 false 不持久化
            //参数3:是否独占队列 true 独占队列 false 不独占
            //参数4:是否在消费完成之后删除队列 true 自动删除 false 不自动删除
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            //发布消息
            //参数1:交换机名称
            //参数2:队列名称
            //参数3:传递消息的额外设置
            //参数4:消息的具体内容
            channel.basicPublish("", QUEUE_NAME, null, MESSAGE.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("消息发送出现异常!");
        }

        RabbitMqUtils.closeConnectionAndChanel(channel, connection);


    }
}
