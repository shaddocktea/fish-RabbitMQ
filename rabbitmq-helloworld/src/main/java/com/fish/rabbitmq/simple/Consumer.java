package com.fish.rabbitmq.simple;

import com.fish.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

/**
 * @ClassName Consumer
 * @Description 消费者
 * @Author PomeloTea
 * @Date 2021/9/11
 */
public class Consumer {

    // 队列名称
    public static final String QUEUE_NAME = "hello";

    public static void main(String[] args) {

        //通过工具类获取连接对象
        Connection connection = RabbitMqUtils.getConnection();
        //获取连接通道
        Channel channel = null;

        // 消息送达时通知的回调接口
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            // 获取消息体
            String strMessage = new String(message.getBody());
            System.out.println(strMessage);
        };

        // 取消消费的回调接口
        CancelCallback cancelCallback = consumerTag -> {
            System.out.println("消费消息被中断...");
        };

        try {
            channel = connection.createChannel();
            //消息消息
            //参数1:消费的队列名称
            //参数2:开始消息的自动确认机制
            //参数3:消息送达时的回调接口
            //参数4:取消消费的回调接口
            channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
