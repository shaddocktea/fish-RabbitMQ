package com.fish.rabbitmq.work.fair;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @ClassName Producer
 * @Description 工作队列(Work模式)-公平分发(Fair Dispatch)-生产者
 * @Author PomeloTea
 * @Date 2021/9/15
 */
public class Producer {

    public static void main(String[] args) {
        // 创建连接工厂
        ConnectionFactory rabbitmqFactory = new ConnectionFactory();
        // 设置IP去连接RabbitMQ
        rabbitmqFactory.setHost("122.9.98.31");
        // 设置端口号
        rabbitmqFactory.setPort(5672);
        // 设置虚拟访问节点
        rabbitmqFactory.setVirtualHost("/");
        // 设置用户名
        rabbitmqFactory.setUsername("admin");
        // 设置密码
        rabbitmqFactory.setPassword("admin");


        Connection connection = null;
        Channel channel = null;
        try {
            // 创建连接
            connection = rabbitmqFactory.newConnection("生产者");
            // 获取信道
            channel = connection.createChannel();

            /*
             声明队列
             Params:
                 queue      队列的名称
                 durable    是否要持久化 durable=false 持久化就是将内存的中的数据保存到磁盘
                 exclusive  声明独占队列，exclusive=true 则该队列仅限于该连接
                 autoDelete 是否自动删除，随着最后一个消费者将消息消费完成之后是否将队列自动删除
                 arguments  携带的附属参数
            */
            channel.queueDeclare("queue-work-polling", false, false, false, null);


            // 发送消息
            for (int i = 0; i < 20; i++) {
                String message = "Hello,Code-Farmer" + i;
                channel.basicPublish("","queue-work-polling", null, message.getBytes());
            }
            System.out.println("消息成功发送!");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("消息发送出现异常....");
        } finally {
            // 关闭信道
            if (null != channel && channel.isOpen()) {
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // 关闭连接
            if (null != connection && connection.isOpen()) {
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }


}
