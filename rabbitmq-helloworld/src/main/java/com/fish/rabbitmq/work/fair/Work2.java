package com.fish.rabbitmq.work.fair;

import com.rabbitmq.client.*;

/**
 * @ClassName Work2
 * @Description 工作队列(Work模式)-公平分发(Fair Dispatch)-消费者2
 * @Author PomeloTea
 * @Date 2021/9/15
 */
public class Work2 {
    public static void main(String[] args) {
        // 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置主机IP
        factory.setHost("122.9.98.31");
        // 设置端口号
        factory.setPort(5672);
        // 设置虚拟访问节点
        factory.setVirtualHost("/");
        // 设置登录用户
        factory.setUsername("admin");
        // 设置密码
        factory.setPassword("admin");

        Connection connection = null;
        Channel channel = null;
        try {
            // 创建连接
            connection = factory.newConnection("消费者-Work2");
            // 创建信道
            channel = connection.createChannel();
            Channel finalChannel = channel;
            finalChannel.basicQos(1);
            // 消息送达时通知的回调接口
            DeliverCallback deliverCallback = (consumerTag, message) -> {
                try {
                    // 获取消息体
                    String strMessage = new String(message.getBody());
                    System.out.println("Work2-收到的消息是：" + strMessage);
                    Thread.sleep(900);
                    finalChannel.basicAck(message.getEnvelope().getDeliveryTag(),false);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };

            // 取消消费的回调接口
            CancelCallback cancelCallback = consumerTag -> {
                System.out.println("消费消息被中断...");
            };

            /*
             消费者消费消息
             参数：
                queue – 消费的队列名称
                autoAck – 设置消息消费成功之后是自动应答还是手动应答 true：自动 false：手动
                DeliverCallback – 传递消息时的回调
                cancelCallback – 消费者取消时的回调
            */
            channel.basicConsume("queue-work-polling", false, deliverCallback, cancelCallback);

            System.out.println("Work2-开始接收消息：");
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭信道
            if (null != channel && channel.isOpen()) {
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //关闭连接
            if (null != connection && connection.isOpen()) {
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
