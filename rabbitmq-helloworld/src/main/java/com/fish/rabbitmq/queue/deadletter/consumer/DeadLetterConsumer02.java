package com.fish.rabbitmq.queue.deadletter.consumer;

import com.fish.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;

/**
 * @ClassName DeadLetterConsumer02
 * @Description 死信队列消费者
 * @Author PomeloTea
 * @Date 2021/9/26
 */
public class DeadLetterConsumer02 {

    //死信队列
    public static final String DEAD_QUEUE = "dead_queue";

    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        System.out.println("等待接收消息中...");
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("死信队列消费者接收到的消息："  + new String(message.getBody(),"UTF-8"));
        };
        //消费消息
        channel.basicConsume(DEAD_QUEUE,true,deliverCallback,consumerTag -> {});
    }
}
