package com.fish.rabbitmq.queue.deadletter.consumer;

import com.fish.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName DeadLetterConsumer01
 * @Description 普通队列消费者
 * @Author PomeloTea
 * @Date 2021/9/25
 */
public class DeadLetterConsumer01 {

    //普通交换机
    public static final String NORMAL_EXCHANGE = "normal_exchange";
    //死信交换机
    public static final String DEAD_EXCHANGE = "dead_exchange";
    //普通队列
    public static final String NORMAL_QUEUE = "normal_queue";
    //死信队列
    public static final String DEAD_QUEUE = "dead_queue";

    public static void main(String[] args) throws Exception {
        //获取连接
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        //声明普通交换机
        channel.exchangeDeclare(NORMAL_EXCHANGE, "direct");
        //声明死信交换机
        channel.exchangeDeclare(DEAD_EXCHANGE, "direct");

        //声明普通队列
        Map<String, Object> map = new HashMap<>();
        //设置正常队列接收消息的最大个数
        //让后面的消息都进入到死信队列中
        //map.put("x-max-length",6);
        //设置与正常队列连接的死信交互机
        map.put("x-dead-letter-exchange", DEAD_EXCHANGE);
        //设置死信RoutingKey
        map.put("x-dead-letter-routing-key", "LiSi");
        channel.queueDeclare(NORMAL_QUEUE, false, false, false, map);
        //声明死信队列
        channel.queueDeclare(DEAD_QUEUE, false, false, false, null);

        //绑定普通交换机和普通队列
        channel.queueBind(NORMAL_QUEUE, NORMAL_EXCHANGE, "ZhangSan");
        //绑定死信交换机和死信队列
        channel.queueBind(DEAD_QUEUE, DEAD_EXCHANGE, "LiSi");


        System.out.println("消费者等待接收消息:");
        //消费消息
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            //拒绝队列中的消息
            //被拒绝后的消息不重新放回原队列中
            //让被拒绝的消息进入到死信队列
            String msg = new String(message.getBody(),"UTF-8");
            if (msg.equals("info5")){
                //拒绝消息
                channel.basicReject(message.getEnvelope().getDeliveryTag(),false);
            }else {
                System.out.println("消费者01接收到的消息是：" + new String(message.getBody(), "UTF-8"));
                channel.basicAck(message.getEnvelope().getDeliveryTag(),false);
            }

        };

        CancelCallback cancelCallback = (consumerTag) -> {
        };

        //如果拒绝消息让其进入到死信队列
        //那么此处不应该开启自动应答
        //而是开启手动应答
        channel.basicConsume(NORMAL_QUEUE, false, deliverCallback, cancelCallback);


    }


}
