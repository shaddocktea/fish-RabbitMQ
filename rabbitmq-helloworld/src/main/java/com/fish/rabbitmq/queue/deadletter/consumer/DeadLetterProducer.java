package com.fish.rabbitmq.queue.deadletter.consumer;

import com.fish.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @ClassName DeadLetterProducer
 * @Description 死信队列生产者
 * @Author PomeloTea
 * @Date 2021/9/26
 */
public class DeadLetterProducer {

    //普通交换机
    public static final String NORMAL_EXCHANGE = "normal_exchange";

    public static void main(String[] args) throws Exception {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();
        //发送消息
        //设置消息的过期时间（单位ms）
        //让过期的消息进入到死信队列中
        //AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();
        for (int i = 1; i <= 10 ; i++) {
            String message = "info" + i;
            channel.basicPublish(NORMAL_EXCHANGE,"ZhangSan",null,message.getBytes());
        }


    }


}
