package com.fish.rabbitmq.confirm;

import com.fish.rabbitmq.utils.RabbitMqUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;
import com.rabbitmq.client.Connection;

import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @ClassName ReleaseAndConfirm
 * @Description 发布确认模式
 * @Author PomeloTea
 * @Date 2021/9/23
 */
public class PublishAndConfirm {

    // 消息发送的次数
    public static final int MESSAGE_COUNT = 1000;

    public static void main(String[] args) throws Exception {
        // 单个发布确认
        // publishMessageIndividually();   // 总耗时:10696ms
        // 批量确认
        // publishMessageBatch();  // 总耗时:265ms
        // 异步批量确认
        publishMessageAsync();  // 总耗时:66ms
    }


    /***
     * @Description 单个确认
     * @Author PomeloTea
     * @Date 2021/9/23
     * @Param
     **/
    public static void publishMessageIndividually() throws Exception {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        // 声明队列
        String queueName = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 6);
        channel.queueDeclare(queueName, true, false, false, null);
        // 开启发布确认
        channel.confirmSelect();
        // 记录开始时间
        long beginTime = System.currentTimeMillis();
        // 批量发送消息
        for (int i = 0; i < MESSAGE_COUNT; i++) {
            String message = String.valueOf(i);
            // 往队列中发送消息
            channel.basicPublish("", queueName, null, message.getBytes());
            // 单个消息马上进行发布确认
            boolean flag = channel.waitForConfirms();
        }
        // 记录结束时间
        long endTime = System.currentTimeMillis();

        System.out.println("总耗时:" + (endTime - beginTime) + "ms");

    }

    /***
     * @Description 批量确认
     * @Author PomeloTea
     * @Date 2021/9/23
     * @Param
     * @return void
     **/
    public static void publishMessageBatch() throws Exception {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        // 声明队列
        String queueName = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 6);
        channel.queueDeclare(queueName, true, false, false, null);
        // 开启发布确认
        channel.confirmSelect();
        // 记录开始时间
        long beginTime = System.currentTimeMillis();
        // 批量发送消息
        // 定义批量数据发送确认频率
        int batchSize = 100;
        for (int i = 1; i <= MESSAGE_COUNT; i++) {
            String message = String.valueOf(i);
            // 往队列中发送消息
            channel.basicPublish("", queueName, null, message.getBytes());

            // 消息每发送100次进行一次确认
            if (i % batchSize == 0) {
                boolean flag = channel.waitForConfirms();
            }

        }
        // 记录结束时间
        long endTime = System.currentTimeMillis();

        System.out.println("总耗时:" + (endTime - beginTime) + "ms");
    }


    /***
     * @Description 异步确认
     * @Author PomeloTea
     * @Date 2021/9/23
     * @Param
     * @return void
     **/
    public static void publishMessageAsync() throws Exception {
        Connection connection = RabbitMqUtils.getConnection();
        Channel channel = connection.createChannel();

        // 声明队列
        String queueName = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 6);
        channel.queueDeclare(queueName, true, false, false, null);
        // 开启发布确认
        channel.confirmSelect();


        // 线程安全有序的哈希表,适合于高并发的情况
        // 将序号于消息进行关联
        // 只要给到序号就能批量删除条目
        // 支持高并发
        ConcurrentSkipListMap<Long, String> skipListMap = new ConcurrentSkipListMap<>();

        // 消息发布确认成功的回调函数
        ConfirmCallback ackCallback = (deliveryTag, multiple) -> {
            if (multiple) {
                // 删除已经确认的消息，剩下的都是未确认的消息
                ConcurrentNavigableMap<Long, String> headMap = skipListMap.headMap(deliveryTag);
                headMap.clear();
            } else {
                skipListMap.remove(deliveryTag);
            }
            System.out.println("确认的消息：" + deliveryTag);
        };

        // 消息发布确认失败的回调函数
        // 参数1：消息的标记
        // 参数2：是否为批量确认
        ConfirmCallback nackCallback = (deliveryTag, multiple) -> {
            String message = skipListMap.get(deliveryTag);
            System.out.println("未确认的消息：" + message);
            System.out.println("未确认的消息Tag：" + deliveryTag);
        };


        // 消息发布确认监听器
        // 参数1：消息发送确认成功的回调函数
        // 参数2：消息发送确认失败的回调函数
        channel.addConfirmListener(ackCallback, nackCallback);
        // 记录开始时间
        long beginTime = System.currentTimeMillis();
        // 批量发送消息
        for (int i = 0; i < MESSAGE_COUNT; i++) {
            String message = String.valueOf(i);
            // 记录发送消息的序号和消息体
            skipListMap.put(channel.getNextPublishSeqNo(), message);
            // 往队列中发送消息
            channel.basicPublish("", queueName, null, message.getBytes());
        }
        // 记录结束时间
        long endTime = System.currentTimeMillis();

        System.out.println("总耗时:" + (endTime - beginTime) + "ms");
    }


}
