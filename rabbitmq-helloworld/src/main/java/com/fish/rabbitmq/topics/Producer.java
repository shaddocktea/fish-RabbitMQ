package com.fish.rabbitmq.topics;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @ClassName Producer
 * @Description 主题(topics)模式/生产者
 * @Author PomeloTea
 * @Date 2021/9/15
 */
public class Producer {

    // 定义交换机名称
    public static final String EXCHANGE_NAME = "topics-exchange";

    public static void main(String[] args) {

        // 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置IP去连接RabbitMQ
        factory.setHost("122.9.98.31");
        // 设置端口号
        factory.setPort(5672);
        // 设置虚拟访问节点
        factory.setVirtualHost("/");
        // 设置用户名
        factory.setUsername("admin");
        // 设置密码
        factory.setPassword("admin");


        Connection connection = null;
        Channel channel = null;
        try {
            // 创建连接
            connection = factory.newConnection("生产者");
            // 获取信道
            channel = connection.createChannel();
            // 声明交换机
            channel.exchangeDeclare(EXCHANGE_NAME, "topic");
            // 声明队列
            channel.queueDeclare("queue-topics-001",true,false,false,null);
            channel.queueDeclare("queue-topics-002",true,false,false,null);
            channel.queueDeclare("queue-topics-003",true,false,false,null);
            // 绑定队列与交换机的关系
            channel.queueBind("queue-topics-001",EXCHANGE_NAME,"*.email.*");
            channel.queueBind("queue-topics-002",EXCHANGE_NAME,"*.sms.*");
            channel.queueBind("queue-topics-003",EXCHANGE_NAME,"#.voice.#");


            // 向交换机发50条消息
            for (int i = 0; i < 50; i++) {
                String message = "hello rabbitmq" + i;
                /*
                 发布消息
                 Params:
                    exchange    需要将消息发布到的交换机名称
                    routingKey  路由的密钥
                    props       消息的其他属性
                    body        消息正文
                */
                channel.basicPublish(EXCHANGE_NAME, "com.email.sms", null, message.getBytes());
                // 间隔20毫秒发送一次消息
                Thread.sleep(20);
                System.out.println("send:" + message);
            }


        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("消息发送出现异常....");
        } finally {
            // 关闭信道
            if (null != channel && channel.isOpen()) {
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // 关闭连接
            if (null != connection && connection.isOpen()) {
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }


}
