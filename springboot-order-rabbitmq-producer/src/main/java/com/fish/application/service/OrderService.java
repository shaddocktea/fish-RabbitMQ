package com.fish.application.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @ClassName OrderService
 * @Description
 * @Author PomeloTea
 * @Date 2021/9/16
 */
@Service
public class OrderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // 定义交换机
    private String exchangeName = "fanout-order-exchange";

    // 定义路由
    private String routeKey = "";

    public void makeOrder(Long userId,Long productId,int number){
        // 模拟用户下单
        String orderNumber = UUID.randomUUID().toString().replaceAll("-","");
        System.out.println("请求发送成功,用户编号："+userId+",订单编号："+orderNumber);
        // 发送订单信息给RabbitMQ
        rabbitTemplate.convertAndSend(exchangeName,routeKey,orderNumber);
    }










}
