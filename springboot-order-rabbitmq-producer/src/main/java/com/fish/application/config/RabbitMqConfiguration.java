package com.fish.application.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName RabbitMqConfiguration
 * @Description 配置类
 * @Author PomeloTea
 * @Date 2021/9/16
 */
@Configuration
public class RabbitMqConfiguration {

    private String exchangeName = "fanout-order-exchange";

    // 声明交换机
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(exchangeName,true,false);
    }
    // 声明队列
    @Bean
    public Queue smsQueue(){
        return new Queue("sms-fanout-queue",true);
    }

    @Bean
    public Queue emailQueue(){
        return new Queue("email-fanout-queue",true);
    }

    // 完成绑定关系（队列和交换机完成绑定）
    @Bean
    public Binding smsBinding(){
        return BindingBuilder.bind(smsQueue()).to(fanoutExchange());
    }
    @Bean
    public Binding emailBinding(){
        return BindingBuilder.bind(emailQueue()).to(fanoutExchange());
    }


}
