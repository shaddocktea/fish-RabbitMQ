package com.fish.application;

import com.fish.application.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationTests {


    @Autowired
    private OrderService orderService;

    @Test
    void contextLoads() {
        orderService.makeOrder(20171004109L,20210916L,100);
    }

}
