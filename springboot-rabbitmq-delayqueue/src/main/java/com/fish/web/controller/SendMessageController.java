package com.fish.web.controller;

import com.fish.web.config.DelayedQueueConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @ClassName SendMessageController
 * @Description 发送延迟消息
 * @Author PomeloTea
 * @Date 2021/9/29
 */
@Slf4j
@RestController
@RequestMapping("/ttl")
public class SendMessageController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/sendMsg/{message}")
    public String sendMessage(@PathVariable String message) {
        log.info("当前时间:{},发送一个消息给两个TTL队列:{}", new Date().toString(), message);
        rabbitTemplate.convertAndSend("X", "XA", "消息来自TTL为10s的队列:" + message);
        rabbitTemplate.convertAndSend("X", "XB", "消息来自TTL为40s的队列:" + message);

        return "消息发送成功:" + new Date().toString();
    }

    @GetMapping("/sendMsg/{message}/{ttlTime}")
    public String sendMessage(@PathVariable String message, @PathVariable final String ttlTime) {
        log.info("当前时间:{},发送一条时长为{}毫秒的TTL信息给队列QC:{}", new Date(), ttlTime, message);
        rabbitTemplate.convertAndSend("X", "XC", message, msg -> {
            msg.getMessageProperties().setExpiration(ttlTime);
            return msg;
        });

        return "死信消息发送成功！";
    }

    @GetMapping("/sendDelayedMsg/{message}/{delayedTime}")
    public String sendMessage(@PathVariable String message,@PathVariable Integer delayedTime){
        log.info("当前时间:{},发送一条时长为{}毫秒信息给延迟队列delayed.queue:{}", new Date().toString(), delayedTime, message);

        rabbitTemplate.convertAndSend(DelayedQueueConfig.DELAYED_EXCHANGE_NAME,DelayedQueueConfig.DELAYED_ROUTING_KEY,message,msg ->{
            //设置发送消息的延迟时间
            msg.getMessageProperties().setDelay(delayedTime);
            return msg;
        });

        return "延迟消息发送成功!";
    }




}
