package com.fish.web.consumer;

import com.fish.web.config.DelayedQueueConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName DelayedQueueConsumer
 * @Description 延迟队列的消费者
 * @Author PomeloTea
 * @Date 2021/10/5
 */
@Slf4j
@Component
public class DelayedQueueConsumer {

    @RabbitListener(queues = DelayedQueueConfig.DELAYED_QUEUE_NAME)
    public void receiveDelayedQueueDMsg(Message message, Channel channel) throws Exception {
        String msg = new String(message.getBody(),"UTF-8");
        log.info("当前时间:{},接收到延迟队列的消息:{}",new Date(),msg);
    }


}
