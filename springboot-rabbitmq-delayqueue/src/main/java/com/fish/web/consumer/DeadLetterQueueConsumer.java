package com.fish.web.consumer;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName DeadLetterQueueConsumer
 * @Description 死信队列消费者
 * @Author PomeloTea
 * @Date 2021/9/29
 */
@Slf4j
@Component
public class DeadLetterQueueConsumer {


    @RabbitListener(queues = "QD")
    public void receiveQueueDMsg(Message message, Channel channel) throws Exception {
        String msg = new String(message.getBody(),"UTF-8");
        log.info("当前时间:{},接收到死信队列的消息:{}",new Date(),msg);

    }


}
