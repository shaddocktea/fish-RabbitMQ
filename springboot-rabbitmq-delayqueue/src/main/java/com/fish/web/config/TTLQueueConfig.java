package com.fish.web.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName TTLQueueConfig
 * @Description TTL队列配置文件类代码
 * @Author PomeloTea
 * @Date 2021/9/29
 */
@Configuration
public class TTLQueueConfig {

    //普通交换机
    public static final String NORMAL_EXCHANGE_X = "X";
    //死信交换机
    public static final String DEAD_LETTER_EXCHANGE_Y = "Y";
    //普通队列
    public static final String NORMAL_QUEUE_A = "QA";
    public static final String NORMAL_QUEUE_B = "QB";
    public static final String NORMAL_QUEUE_C = "QC";
    //死信队列
    public static final String DEAD_LETTER_QUEUE = "QD";


    //声明普通交换机
    @Bean("xExchange")
    public DirectExchange xExchange(){
        return new DirectExchange(NORMAL_EXCHANGE_X);
    }

    //声明死信交换机
    @Bean("yExchange")
    public DirectExchange yExchange(){
        return new DirectExchange(DEAD_LETTER_EXCHANGE_Y);
    }

    //声明普通队列A
    @Bean("queueA")
    public Queue queueA(){
        Map<String,Object> map = new HashMap<String, Object>();
        //绑定死信交换机和队列的关系
        map.put("x-dead-letter-exchange",DEAD_LETTER_EXCHANGE_Y);
        //设置死信交换机的路由
        map.put("x-dead-letter-routing-key","YD");
        //设置队列的过期时间(单位:ms)
        map.put("x-message-ttl",10000);
        return QueueBuilder.durable(NORMAL_QUEUE_A).withArguments(map).build();
    }

    //声明普通队列B
    @Bean("queueB")
    public Queue queueB(){
        Map<String,Object> map = new HashMap<String, Object>();
        //绑定死信交换机和队列的关系
        map.put("x-dead-letter-exchange",DEAD_LETTER_EXCHANGE_Y);
        //设置死信交换机的路由
        map.put("x-dead-letter-routing-key","YD");
        //设置队列的过期时间(单位:ms)
        map.put("x-message-ttl",40000);
        return QueueBuilder.durable(NORMAL_QUEUE_B).withArguments(map).build();
    }

    //声明普通队列C
    @Bean("queueC")
    public Queue queueC(){
        Map<String,Object> map = new HashMap<String, Object>();
        //绑定死信交换机和队列的关系
        map.put("x-dead-letter-exchange",DEAD_LETTER_EXCHANGE_Y);
        //设置死信交换机的路由
        map.put("x-dead-letter-routing-key","YD");
        return QueueBuilder.durable(NORMAL_QUEUE_C).withArguments(map).build();
    }

    //声明死信队列D
    @Bean("queueD")
    public Queue queueD(){
        return QueueBuilder.durable(DEAD_LETTER_QUEUE).build();
    }


    //绑定队列A和交换机X的关系
    @Bean
    public Binding queueABinding(){
        return BindingBuilder.bind(queueA()).to(xExchange()).with("XA");
    }

    //绑定队列B和交换机X的关系
    @Bean
    public Binding queueBBinding(){
        return BindingBuilder.bind(queueB()).to(xExchange()).with("XB");
    }

    //绑定队列C和交换机X的关系
    @Bean
    public Binding queueCBinding(){
        return BindingBuilder.bind(queueC()).to(xExchange()).with("XC");
    }


    //绑定死信交换机Y和队列D的关系
    @Bean
    public Binding queueDBinding(){
        return BindingBuilder.bind(queueD()).to(yExchange()).with("YD");
    }















}
