package com.fish.web.service.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName FanoutEmailConsumer
 * @Description
 * @Author PomeloTea
 * @Date 2021/9/16
 */
@Component
@RabbitListener(queues = {"email-fanout-queue"})
public class FanoutEmailConsumer {


    @RabbitHandler
    public void receiveEmailMessage(String message) {
        System.out.println("email ----接收到的订单信息是:" + message);
    }


}
