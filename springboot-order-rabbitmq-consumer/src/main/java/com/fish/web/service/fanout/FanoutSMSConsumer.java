package com.fish.web.service.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName FanoutSMSConsumer
 * @Description 消费者
 * @Author PomeloTea
 * @Date 2021/9/16
 */
@Component
@RabbitListener(queues = {"sms-fanout-queue"})
public class FanoutSMSConsumer {

    @RabbitHandler
    public void receiveSMSMessage(String message){
        System.out.println("sms ----接收到的订单信息是:" + message);
    }


}
